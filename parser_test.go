package iprep

import (
	"fmt"
	"io"
	"reflect"
	"testing"
	"time"
)

// Minimal test of the log parser.
func TestLogParser(t *testing.T) {
	testLog := `example.com:80 178.255.215.79 - - [13/Jan/2015:22:48:11 +0000] "GET /robots.txt HTTP/1.1" 200 51 "-" "Mozilla/5.0 (compatible; Exabot/3.0; +http://www.exabot.com/go/robot)"`
	expectedResult := &Log{
		Addr:      "178.255.215.79",
		Host:      "example.com:80",
		Method:    "GET",
		URL:       "/robots.txt",
		UserAgent: "Mozilla/5.0 (compatible; Exabot/3.0; +http://www.exabot.com/go/robot)",
		Referer:   "-",
		Status:    200,
		Stamp:     time.Date(2015, time.January, 13, 22, 48, 11, 0, time.UTC),
	}

	parser := logParserFunc(extendedLogParse)
	result := parser.Parse([]byte(testLog))
	if result == nil {
		t.Fatal("Parse() returned nil")
	}

	if !reflect.DeepEqual(result, expectedResult) {
		t.Fatalf("bad result from Parse(): got=%+v, expected=%+v", result, expectedResult)
	}

}

func writeRandomLogs(w io.Writer, count int) {
	for i := 0; i < count; i++ {
		fmt.Fprintf(w, "example.com:80 178.255.215.79 - - [%s] \"GET /robots.txt HTTP/1.1\" 200 51 \"-\" \"Mozilla/5.0 (compatible; Exabot/3.0; +http://www.exabot.com/go/robot)\"\n", time.Now().Format(httpLogTimeFormat))
	}
}

// LogParser benchmark - again just to double-check, performance is
// basically dominated by Go's regexp library.
func BenchmarkLogParser(b *testing.B) {
	parser, _ := NewLogParser("extended")

	r, w := io.Pipe()
	go func() {
		writeRandomLogs(w, b.N)
		w.Close()
	}()

	for range Analyze(r, parser) {
		// Just consume output, do nothing.
	}
}
