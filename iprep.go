package iprep

import (
	"fmt"
	"hash/fnv"
	"io"
	"log"
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/clarkduvall/hyperloglog"
)

// HyperLogLog++ precision. Determines the accuracy/memory usage
// threshold. It should be large enough for
// TestCardinality_DetectionHigh to pass
const hllPrecision = 8

// Minimum request rate to be considered by iprep. Currently set to
// one request every 5 minutes.
var minQps float64 = (1.0 / 300)

// Metrics that we can generate for a specific IP.
type ipMetrics struct {
	Addr                string
	BadQPSLongTerm      float64
	GoodQPSLongTerm     float64
	BadQPSShortTerm     float64
	GoodQPSShortTerm    float64
	URLCardinalityCount uint64
}

// Internal state about an IP.
type ipInfo struct {
	lastSeen       time.Time
	goodQPS        *RateEstimator
	badQPS         *RateEstimator
	urlCardinality *hyperloglog.HyperLogLogPlus
}

func newIPInfo() *ipInfo {
	h, _ := hyperloglog.NewPlus(hllPrecision)
	return &ipInfo{
		goodQPS:        newRateEstimator(),
		badQPS:         newRateEstimator(),
		urlCardinality: h,
	}
}

// Metrics returns the summarized metrics associated with this IP.
func (i *ipInfo) Metrics() ipMetrics {
	return ipMetrics{
		BadQPSShortTerm:     i.badQPS.Rate(shortRangeBucket),
		BadQPSLongTerm:      i.badQPS.Rate(longRangeBucket),
		GoodQPSShortTerm:    i.goodQPS.Rate(shortRangeBucket),
		GoodQPSLongTerm:     i.goodQPS.Rate(longRangeBucket),
		URLCardinalityCount: i.urlCardinality.Count(),
	}
}

// IPRep holds the main business logic for the program, receiving logs
// from the Analyze function, maintaining statistics about client IP
// addresses, and eventually triggering actions.
type IPRep struct {
	ips          map[string]*ipInfo
	whitelist    map[string]struct{}
	badPatterns  PatternList
	blockCommand string

	// Count of processed log entries.
	Processed int64
}

// NewIPRep creates a new IPRep object.
func NewIPRep(badPatterns PatternList, ipWhitelist []string, blockCommand string) *IPRep {
	whitelist := make(map[string]struct{})
	for _, ip := range ipWhitelist {
		whitelist[ip] = struct{}{}
	}
	return &IPRep{
		ips:          make(map[string]*ipInfo),
		whitelist:    whitelist,
		badPatterns:  badPatterns,
		blockCommand: blockCommand,
	}
}

// How often we should clear up old entries in 'ips'.
var expungePeriod = 7200 * time.Second

// Run analysis, reading Log objects from c until it is closed.
func (r *IPRep) Run(c <-chan *Log) {
	expungeDeadline := time.Now().UTC().Add(expungePeriod)

	// Process an incoming log. Note that we rely on the
	// timestamps given in the log itself and never actually call
	// time.Now().
	for l := range c {
		r.Processed++

		if _, ok := r.whitelist[l.Addr]; ok {
			continue
		}

		info, ok := r.ips[l.Addr]
		if !ok {
			info = newIPInfo()
			r.ips[l.Addr] = info
		}

		info.lastSeen = l.Stamp
		info.goodQPS.Incr(l.Stamp)
		urlhash := fnv.New64a()
		io.WriteString(urlhash, l.URL)
		info.urlCardinality.Add(urlhash)

		if r.badPatterns.MatchAny(l) {
			info.badQPS.Incr(l.Stamp)
			r.checkIP(l.Addr)
		}

		// Periodically trigger garbage collection on the
		// per-IP stats.
		if l.Stamp.After(expungeDeadline) {
			expungeDeadline = l.Stamp.Add(expungePeriod)
			r.expunge()
		}
	}
}

// Expunge old entries from the IP reputation table. In order to
// actually release memory, we can't just call delete() but must
// replace the old map with a new one.
func (r *IPRep) expunge() {
	deadline := time.Now().Add(-86400 * time.Second)
	newIps := make(map[string]*ipInfo)
	for ip, info := range r.ips {
		if info.lastSeen.After(deadline) {
			newIps[ip] = info
		}
	}
	r.ips = newIps
}

// CheckIP is called on a bad match, and it performs some action if
// the IP should be banned.
func (r *IPRep) checkIP(addr string) {
	info := r.ips[addr]
	if info == nil {
		return
	}

	badQps := info.badQPS.Rate(1)
	allQps := info.goodQPS.Rate(1)
	cardinality := info.urlCardinality.Count()

	// Ignore IP if the sample size is low.
	if allQps < minQps {
		return
	}

	// Ignore IP if it has no bad qps.
	if badQps < minQps {
		return
	}

	// Brute forcers have very low URL cardinality.
	if cardinality > 2 {
		return
	}

	// Block the IP.
	log.Printf("bad IP: %s (qps[10m]=%f, cardinality=%d)", addr, badQps, cardinality)
	r.blockIP(addr)
}

// Block the desired address.
func (r *IPRep) blockIP(addr string) {
	cmd := exec.Command("/bin/sh", "-c", strings.Replace(r.blockCommand, "%s", addr, -1))
	cmd.Run()
}

type ipMetricsList []ipMetrics

func (l ipMetricsList) Len() int      { return len(l) }
func (l ipMetricsList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l ipMetricsList) Less(i, j int) bool {
	return l[i].GoodQPSLongTerm < l[j].GoodQPSLongTerm
}

// Dump statistics about all known IP addresses to stdout.
func (r *IPRep) Dump() {
	var metrics []ipMetrics

	for ip, info := range r.ips {
		m := info.Metrics()
		m.Addr = ip
		metrics = append(metrics, m)
	}

	sort.Sort(ipMetricsList(metrics))

	for _, m := range metrics {
		fmt.Printf("%s\t%d\t%f\t%f\t%f\t%f\n", m.Addr, m.URLCardinalityCount, m.GoodQPSLongTerm, m.GoodQPSShortTerm, m.BadQPSLongTerm, m.BadQPSShortTerm)
	}
}
