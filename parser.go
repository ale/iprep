package iprep

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Log contains parsed fields for a single request log entry.
type Log struct {
	Addr      string
	Host      string
	Method    string
	URL       string
	UserAgent string
	Referer   string
	Status    int
	Stamp     time.Time
}

// LogParser is the interface for something that can parse a log line
// into a Log object.
type LogParser interface {
	Parse([]byte) *Log
}

type logParserFunc func([]byte) *Log

func (f logParserFunc) Parse(line []byte) *Log {
	return f(line)
}

var httpLogTimeFormat = "02/Jan/2006:15:04:05 -0700"

// Create a Log object with the specified components. If there is any
// error in decoding or parsing the fields, nil is returned.
func mkLog(host, addr, date, request, status, referer, userAgent string) *Log {
	stamp, err := time.Parse(httpLogTimeFormat, date)
	if err != nil {
		return nil
	}

	qstr := strings.Fields(request)
	if len(qstr) < 2 {
		return nil
	}

	statusCode, err := strconv.Atoi(status)
	if err != nil {
		return nil
	}

	return &Log{
		Host:      host,
		Addr:      strings.TrimPrefix(addr, "::ffff:"),
		Method:    qstr[0],
		URL:       qstr[1],
		Status:    statusCode,
		Referer:   referer,
		UserAgent: userAgent,
		Stamp:     stamp.UTC(),
	}
}

var commonLogFormatRx = regexp.MustCompile(
	`^([^ ]*) ([^ ]*) ([^ ]*) \[([^]]*)\] "([^"]*)" ([^ ]*) ([^ ]*) "([^"]*)" "([^"]*)"`)

func commonLogParse(line []byte) *Log {
	fields := commonLogFormatRx.FindSubmatch(line)
	if fields == nil {
		return nil
	}

	return mkLog("", string(fields[1]), string(fields[4]), string(fields[5]), string(fields[6]), string(fields[8]), string(fields[9]))
}

var extendedLogFormatRx = regexp.MustCompile(
	`^([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) \[([^]]*)\] "([^"]*)" ([^ ]*) ([^ ]*) "([^"]*)" "([^"]*)"`)

func extendedLogParse(line []byte) *Log {
	fields := extendedLogFormatRx.FindSubmatch(line)
	if fields == nil {
		return nil
	}

	return mkLog(string(fields[1]), string(fields[2]), string(fields[5]), string(fields[6]), string(fields[7]), string(fields[9]), string(fields[10]))
}

// NewLogParser creates a new LogParser for the specified log
// format. At the moment only the 'common' and 'extended' formats (in
// reference to the equivalent Apache standard log formats) are
// supported.
func NewLogParser(format string) (LogParser, error) {
	switch format {
	case "common":
		return logParserFunc(commonLogParse), nil
	case "ext", "extended":
		return logParserFunc(extendedLogParse), nil
	default:
		return nil, fmt.Errorf("unknown log format '%s'", format)
	}
}

// Analyze the given input stream of request logs with a
// LogParser. Resulting Log objects are sent over the returned
// channel. The channel will be closed when the input is done.
func Analyze(r io.Reader, parser LogParser) <-chan *Log {
	c := make(chan *Log, 1000)

	go func() {
		defer close(c)
		s := bufio.NewScanner(r)
		for s.Scan() {
			l := parser.Parse(s.Bytes())
			if l != nil {
				c <- l
			}
		}
		if err := s.Err(); err != nil {
			log.Printf("ERROR: reading input: %v", err)
		}
	}()

	return c
}
