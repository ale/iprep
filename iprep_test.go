package iprep

import (
	"fmt"
	"hash/fnv"
	"testing"

	"github.com/clarkduvall/hyperloglog"
)

func makeRequests(hll *hyperloglog.HyperLogLogPlus, count, cardinality int) {
	for i := 0; i < count; i++ {
		h := fnv.New64a()
		fmt.Fprintf(h, "test%06d", i%cardinality)
		hll.Add(h)
	}
}

// Test that the cardinality estimator can detect low-cardinality
// flows reliably. The hllPrecision value should be high enough for
// this test to pass with exact cardinality estimates.
func TestCardinality_ReliableDetectionLow(t *testing.T) {
	for _, cardinality := range []int{1, 2, 5, 10} {
		hll, _ := hyperloglog.NewPlus(hllPrecision)
		makeRequests(hll, 1000, cardinality)
		count := hll.Count()
		if int(count) != cardinality {
			t.Errorf("expected cardinality = %d, got %d", cardinality, count)
		}
	}
}

// Test that the cardinality estimator can detect high-cardinality
// flows reliably. The test is weaker, we just expect Count() to
// return > 2.
func TestCardinality_DetectionHigh(t *testing.T) {
	precisionLimit := 1<<(hllPrecision-1) + 1
	for cardinality := precisionLimit; cardinality < 10000; cardinality *= 2 {
		hll, _ := hyperloglog.NewPlus(hllPrecision)
		makeRequests(hll, 10000, cardinality)
		count := hll.Count()
		if int(count) < 3 {
			t.Errorf("expected cardinality (actual = %d) too low, got %d", cardinality, count)
		}
		t.Logf("cardinality estimate (%d) = %d", cardinality, count)
	}
}
