package iprep

import (
	"math"
	"testing"
	"time"
)

func TestRateEstimator_Incr(t *testing.T) {
	timeBuckets = []time.Duration{
		10 * time.Millisecond,
		20 * time.Millisecond,
		100 * time.Millisecond,
		3600 * time.Second,
	}
	e := newRateEstimator()

	compareRates := func(tag string, expected []float64) {
		for i := 0; i < len(timeBuckets); i++ {
			delta := e.Rate(i) - expected[i]
			if math.Abs(delta) > 0.1 {
				t.Errorf("%s: expected rate for t=%s is %f, got %f", tag, timeBuckets[i], expected[i], e.Rate(i))
			}
		}
	}

	tt := time.Now()
	for i := 0; i < 10; i++ {
		e.Incr(tt)
		tt = tt.Add(10 * time.Millisecond)
	}
	compareRates("fast", []float64{100, 100, 0, 0})
	for i := 0; i < 10; i++ {
		e.Incr(tt)
		tt = tt.Add(10 * time.Millisecond)
	}
	compareRates("fast2", []float64{100, 100, 100, 0})
	for i := 0; i < 10; i++ {
		e.Incr(tt)
		tt = tt.Add(100 * time.Millisecond)
	}
	// rate estimators can't see less than 1 hit per interval.
	compareRates("slow", []float64{100, 50, 10, 0})
}

// Pointless benchmark, ony used to profile RateEstimator code.
func BenchmarkRateEstimator_Incr(b *testing.B) {
	timeBuckets = []time.Duration{
		10 * time.Millisecond,
		20 * time.Millisecond,
		100 * time.Millisecond,
		200 * time.Millisecond,
	}
	e := newRateEstimator()

	for i := 0; i < b.N; i++ {
		e.Incr(time.Now())
	}
}
