package main

import (
	"bufio"
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"git.autistici.org/ale/iprep"
)

var (
	logFormat     = flag.String("format", "common", "log format (common, extended)")
	patternFile   = flag.String("patterns", "", "read bad patterns from this file")
	whitelistFile = flag.String("whitelist", "", "read IP whitelist from this file")
	blockCommand  = flag.String("action", "", "command to invoke to block an IP, which will be passed as the first argument")
)

func readIPWhitelist(path string) ([]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var out []string
	for s := bufio.NewScanner(f); s.Scan(); {
		line := strings.TrimSpace(s.Text())
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		out = append(out, line)
	}
	return out, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Sanity check the --action option.
	if !strings.Contains(*blockCommand, "%s") {
		log.Fatal("The --action argument should contain a '%s' token to be replaced by the IP")
	}

	// Read bad URL patterns.
	var patterns iprep.PatternList
	if *patternFile != "" {
		f, err := os.Open(*patternFile)
		if err != nil {
			log.Fatal(err)
		}
		patterns, err = iprep.LoadPatterns(f)
		if err != nil {
			log.Fatal(err)
		}
		f.Close()
	}

	// Read from file (first argument), or standard input.
	var input io.Reader
	if flag.NArg() < 1 {
		input = os.Stdin
	} else {
		logfile := flag.Arg(0)
		f, err := os.Open(logfile)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		input = f
	}

	// Always include localhost in the IP whitelist.
	ipWhitelist := []string{"127.0.0.1", "::1"}
	if *whitelistFile != "" {
		wl, err := readIPWhitelist(*whitelistFile)
		if err != nil {
			log.Fatal(err)
		}
		ipWhitelist = append(ipWhitelist, wl...)
	}

	parser, err := iprep.NewLogParser(*logFormat)
	if err != nil {
		log.Fatal(err)
	}

	rep := iprep.NewIPRep(patterns, ipWhitelist, *blockCommand)

	// Print out a summary on exit.
	end := make(chan os.Signal)
	defer close(end)
	go func() {
		<-end
		rep.Dump()
		os.Exit(0)
	}()
	signal.Notify(end, syscall.SIGINT, syscall.SIGTERM)

	start := time.Now()

	rep.Run(iprep.Analyze(input, parser))
	elapsed := time.Since(start)
	log.Printf("processed %d records in %s (%f logs/s)", rep.Processed, elapsed, float64(rep.Processed)/elapsed.Seconds())
}
