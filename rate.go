package iprep

import "time"

const (
	shortRangeBucket = 0
	longRangeBucket  = 1
)

// Different time buckets for rate estimation.
var timeBuckets = []time.Duration{
	10 * time.Second,
	// 300 * time.Second,
	// 1800 * time.Second,
	86400 * time.Second,
}

// RateEstimator is a rate counter that keeps tracks of rates over
// separate time ranges (cf. timeBuckets above for their definition).
type RateEstimator struct {
	stamps   []time.Time
	counters []int64
	accum    []int64
}

func newRateEstimator() *RateEstimator {
	return &RateEstimator{
		stamps:   make([]time.Time, len(timeBuckets)),
		counters: make([]int64, len(timeBuckets)),
		accum:    make([]int64, len(timeBuckets)),
	}
}

// Incr increments the counter by one.
func (e *RateEstimator) Incr(now time.Time) {
	for i := 0; i < len(timeBuckets); i++ {
		if now.Sub(e.stamps[i]) >= timeBuckets[i] {
			e.stamps[i] = now
			e.counters[i] = e.accum[i]
			e.accum[i] = 1
		} else {
			e.accum[i]++
		}
	}
}

// Rate of requests in the chosen time bucket.
func (e *RateEstimator) Rate(bucket int) float64 {
	return float64(e.counters[bucket]) / timeBuckets[bucket].Seconds()
}
