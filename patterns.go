package iprep

import (
	"encoding/json"
	"io"
	"regexp"
	"strconv"
)

// FieldMatch is a match on a single field of the request.
type FieldMatch struct {
	Field    string `json:"field"`
	MatchRx  string `json:"match"`
	compiled *regexp.Regexp
}

func (f *FieldMatch) Match(l *Log) bool {
	var s string
	switch f.Field {
	case "host":
		s = l.Host
	case "method":
		s = l.Method
	case "url":
		s = l.URL
	case "user-agent":
		s = l.UserAgent
	case "referer":
		s = l.Referer
	case "status":
		s = strconv.Itoa(l.Status)
	}
	return f.compiled.FindStringIndex(s) != nil
}

// Pattern is a single rule, applying a logical AND to all its
// matches.
type Pattern struct {
	Matches []*FieldMatch `json:"matches"`
}

func (p *Pattern) Match(l *Log) bool {
	for _, m := range p.Matches {
		if !m.Match(l) {
			return false
		}
	}
	return true
}

func (p *Pattern) compile() error {
	for _, m := range p.Matches {
		var err error
		m.compiled, err = regexp.Compile(m.MatchRx)
		if err != nil {
			return err
		}
	}
	return nil
}

// A PatternList is just a list of Patterns.
type PatternList []*Pattern

func (pl PatternList) MatchAny(l *Log) bool {
	for _, p := range pl {
		if p.Match(l) {
			return true
		}
	}
	return false
}

// LoadPatterns reads a list of patterns from an input stream.
func LoadPatterns(r io.Reader) (PatternList, error) {
	var patterns []*Pattern
	if err := json.NewDecoder(r).Decode(&patterns); err != nil {
		return nil, err
	}
	for _, p := range patterns {
		if err := p.compile(); err != nil {
			return nil, err
		}
	}
	return PatternList(patterns), nil
}
